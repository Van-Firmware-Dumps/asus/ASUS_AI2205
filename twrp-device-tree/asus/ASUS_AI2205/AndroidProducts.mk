#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_ASUS_AI2205.mk

COMMON_LUNCH_CHOICES := \
    omni_ASUS_AI2205-user \
    omni_ASUS_AI2205-userdebug \
    omni_ASUS_AI2205-eng
